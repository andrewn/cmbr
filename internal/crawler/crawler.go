package crawler

import (
	"fmt"
	"net/url"
	"os"
	"time"

	colly "github.com/gocolly/colly/v2"
	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-com/gl-infra/cmbr/internal/authenticator"
)

// Options encapsulates the options for the crawler.
type Options struct {
	Parallelism int
	Instances   []string
	Debug       bool
	Traffic     string

	// Environment configuration
	Username  string
	Password  string
	UserAgent string
	Cookie    string
	Token     string
}

type crawler struct {
	options                 *Options
	collector               *colly.Collector
	gitlabSessionCookie     string
	basicAuth               string
	crawlingCounterChan     chan<- struct{}
	assetFailureCounterChan chan<- struct{}
	assetTotalCounterChan   chan<- struct{}
}

const (
	web      = "web"
	assets   = "assets"
	api      = "api"
	git      = "git"
	pages    = "pages"
	registry = "registry"
)

func (f *crawler) prepareGitlabSessionCookie(c *colly.Collector, hostURL string) (string, error) {
	gitlabSessionCookie := ""

	if f.options.Cookie != "" {
		gitlabSessionCookie += f.options.Cookie + ";"
	}

	// Authorize for Web related traffic
	if (f.options.Traffic == web || f.options.Traffic == registry) && f.options.Username != "" && f.options.Password != "" {
		var signedInUserCookie string

		signedInUserCookie, err := authenticator.SignIn(hostURL, f.options.Username, f.options.Password)
		if err != nil {
			return "", fmt.Errorf("unable to sign in: %w", err)
		}

		gitlabSessionCookie += signedInUserCookie
	}

	return gitlabSessionCookie, nil
}

func (f *crawler) checkInstanceAvailability(c *colly.Collector, hostURL string) {
	err := authenticator.CheckInstance(hostURL)
	if err != nil {
		log.WithFields(log.Fields{"host": hostURL, "err": err}).Fatalf("unable to check instance availability")
	}
}

func (f *crawler) crawl() error {
	instance0URL, err := url.Parse(f.options.Instances[0])
	if err != nil {
		return fmt.Errorf("unable to parse instance URL: %w", err)
	}

	host := instance0URL.Host
	hostURL := fmt.Sprintf("%s://%s", instance0URL.Scheme, host)
	f.crawlingCounterChan = counter("crawling...", 100)

	if f.options.Traffic == assets {
		f.assetFailureCounterChan = counter("asset 404...", 1)
		f.assetTotalCounterChan = counter("crawling assets...", 10)
	}

	c := colly.NewCollector(
		colly.IgnoreRobotsTxt(),
		colly.AllowedDomains(host),
		colly.Async(true),
	)

	// Increase timeout to account for slow pages
	c.SetRequestTimeout(30 * time.Second)

	// Allow revisits.
	// If the instance is big it will never revisit URLs,
	// if it's small we don't want CMBR to stop producing the traffic until the end of -duration period
	c.AllowURLRevisit = true

	f.collector = c

	limit := colly.LimitRule{DomainGlob: "*", Parallelism: f.options.Parallelism}
	// Throttle to bypass Pages rate limit which is 20 RPS by default
	if f.options.Traffic == pages {
		limit.Delay = 1 * time.Second
	}

	err = c.Limit(&limit)
	if err != nil {
		return fmt.Errorf("unable to set gocolly limit: %w", err)
	}

	if os.Getenv("SKIP_CHECK_INSTANCE_AVAILABILITY") == "" {
		f.checkInstanceAvailability(c, hostURL)
	}

	f.gitlabSessionCookie, err = f.prepareGitlabSessionCookie(c, hostURL)
	if err != nil {
		return err
	}

	f.basicAuth = authenticator.GenerateBasicAuth(f.options.Traffic, f.options.Username, f.options.Password)

	registerHooks(f, c)

	for _, v := range f.options.Instances {
		err = c.Visit(v)
		if err != nil {
			log.WithError(err).WithFields(log.Fields{"url": v}).Error("failed to visit starting instance")
		}
	}

	return nil
}

// Crawl will crawl a gitlab instance using the configured values.
func Crawl(options *Options) error {
	c := crawler{options: options}

	return c.crawl()
}

func registerHooks(f *crawler, c *colly.Collector) {
	if f.options.Traffic == assets {
		c.OnHTML("link[href*='.css']", f.onCSSLink)
		c.OnHTML("script[src]", f.onJSLink)
	}

	c.OnHTML("a[href]", f.onHTMLLink)
	c.OnHTML("img[src]", f.onHTMLImage)
	c.OnResponse(f.onResponse)
	c.OnRequest(f.onRequest)
	c.OnError(f.onError)
}
