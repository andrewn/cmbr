package crawler

import (
	"fmt"
	"net/url"
	"strings"

	colly "github.com/gocolly/colly/v2"
)

func (f *crawler) checkForRegistryRepoAPI(c *colly.Collector, baseURL *url.URL, d map[string]interface{}) {
	projectRepoURL := getStringFromJSONMap(d, "http_url_to_repo")
	if !strings.Contains(projectRepoURL, ".git") {
		return
	}

	projectWebURL := getStringFromJSONMap(d, "web_url")

	packagesURL, err := url.Parse(fmt.Sprintf("%s/-/packages", projectWebURL))
	if err != nil {
		return
	}

	containerRegistryURL, err := url.Parse(fmt.Sprintf("%s/container_registry", projectWebURL))
	if err != nil {
		return
	}

	infrastructureRegistryURL, err := url.Parse(fmt.Sprintf("%s/-/infrastructure_registry", projectWebURL))
	if err != nil {
		return
	}

	f.addLink(c, packagesURL)
	f.addLink(c, containerRegistryURL)
	f.addLink(c, infrastructureRegistryURL)
}
