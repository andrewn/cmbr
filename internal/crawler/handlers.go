package crawler

import (
	"net/http"
	"net/url"
	"strings"

	colly "github.com/gocolly/colly/v2"
	log "github.com/sirupsen/logrus"
	"github.com/tomnomnom/linkheader"
)

func (f *crawler) onHTMLLink(e *colly.HTMLElement) {
	f.onElement(e, "href")
}

func (f *crawler) onCSSLink(e *colly.HTMLElement) {
	f.onElement(e, "href")
}

// Visit images to test the image scaler.
func (f *crawler) onHTMLImage(e *colly.HTMLElement) {
	f.onElement(e, "src")
}

func (f *crawler) onJSLink(e *colly.HTMLElement) {
	f.onElement(e, "src")
}

func (f *crawler) onElement(e *colly.HTMLElement, attr string) {
	link := e.Attr(attr)
	if link == "" {
		return
	}

	fullLink := e.Request.AbsoluteURL(link)

	u, err := url.Parse(fullLink)
	if err != nil {
		return
	}

	f.addLink(f.collector, u)
}

func (f *crawler) onResponse(r *colly.Response) {
	f.traverseJSON(f.collector, r)

	f.crawlingCounterChan <- struct{}{}

	if isStaticAsset(r.Request) {
		f.assetTotalCounterChan <- struct{}{}
	}

	// Walk links last
	links := linkheader.Parse(r.Headers.Get("link"))
	for _, link := range links {
		u, err := url.Parse(link.URL)
		if err == nil {
			f.addLink(f.collector, u)
		}
	}

	// Paginate to the next page
	if f.options.Traffic == git || f.options.Traffic == registry {
		nextPage := r.Headers.Get("X-Next-Page")
		if nextPage != "" {
			q := r.Request.URL.Query()
			q.Set("page", nextPage)
			r.Request.URL.RawQuery = q.Encode()
			f.addLink(f.collector, r.Request.URL)
		}
	}
}

func (f *crawler) onRequest(r *colly.Request) {
	if f.options.Token != "" {
		r.Headers.Set("Private-Token", f.options.Token)
	}

	// To bypass CloudFlare check
	if f.options.UserAgent != "" {
		r.Headers.Set("user-agent", f.options.UserAgent)
	}

	// To use authorized user session
	if f.gitlabSessionCookie != "" {
		r.Headers.Set("cookie", f.gitlabSessionCookie)
	}

	// To auth for git requests
	if f.basicAuth != "" {
		r.Headers.Set("Authorization", f.basicAuth)
	}

	log.WithField("url", r.URL).Debug("Creating request for ", r.URL.String())
}

// Set error handler.
func (f *crawler) onError(r *colly.Response, err error) {
	gitlabServer := ""
	requestID := ""

	if r.Headers != nil {
		gitlabServer = r.Headers.Get("Gitlab-Sv")
		requestID = r.Headers.Get("X-Request-Id")
	}

	if isStaticAsset(r.Request) {
		f.assetTotalCounterChan <- struct{}{}

		if r.StatusCode == http.StatusNotFound {
			f.assetFailureCounterChan <- struct{}{}
		}
	}

	log.WithError(err).WithFields(log.Fields{
		"url":            r.Request.URL.String(),
		"status":         r.StatusCode,
		"gitlab_server":  gitlabServer,
		"correlation_id": requestID,
	}).Error("request failed")
}

func isStaticAsset(r *colly.Request) bool {
	requestPath := r.URL.Path

	if strings.HasSuffix(requestPath, ".js") || strings.HasSuffix(requestPath, ".css") {
		return true
	}

	return false
}
