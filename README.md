# `cmbr`: Cosmic Microwave Background Radiation

A simple tool to generate background traffic for staging.

[![cmbr](img/cmbr.png)](https://en.wikipedia.org/wiki/Cosmic_microwave_background)

## Why?

`cmbr` is a tiny command-line webcrawler, based on the [Colly library](https://github.com/gocolly/colly).

**Note**: this is not a general-purpose webcrawler, but rather one designed to crawl a GitLab instance (specifically, `staging.gitlab.com`). The purpose of this is to increase the background traffic volumes on staging, which tend to be too low to be useful for SLIs.

-----------------------------

### Goals

1. Navigate a broad set of read-only endpoints on GitLab's Web and API services.
1. Avoid rate-limiting (429) and other 4xx client error requests where possible.
1. Provide a steady volume of background traffic with as few spikes as possible.

### Non-Goals

1. Direct instrumentation: we will not measure the performance of `cmbr` directly. The measurements are done through GitLab.com's SLO monitoring platform.
1. Requests that modify or delete data are out of scope for now (only GET requests).

## Usage

The tool has the following flags:

1. `-concurrency` [default `10`] the number of concurrent requests `cmbr` will issue.
1. `-duration` [default `60s`] the time `cmbr` will run for before terminating.
1. `-debug` [default `false`] emits more debugging information.
1. `-traffic` [default `web`] limit crawling to specific endpoints. Options: `web`, `assets`, `api`, `git`, `pages`, `registry`.
1. `-instance` [required, no default] the seed URL from which the crawler will start navigating the GitLab instance.
1. `-output` [default, `text`] the log output format to use. Can be `json`, `text` or `color`.

**Note**: The crawler will limit itself to the same domain as `-instance` URL. It will not fetch any resources away from that domain.

Environment variables:

- `GITLAB_TOKEN` - Token used for API authentication by setting the `Private-Token` header. To obtain a token, follow [Personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) for the target environment.
- `GITLAB_USERNAME` - Username that will be used to authorize as specified user in Web requests.
- `GITLAB_PASSWORD` - Password that will be used to authorize as specified user in Web requests.
- `GITLAB_USER_AGENT` - Provide agent to bypass CloudFlare check on Staging environment.
  - Required when running against Staging. Agent value is same as [`GITLAB_QA_USER_AGENT`](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/blob/main/runbooks/rotating-credentials.md#gitlab_qa_user_agent) used by [Test Platform](https://about.gitlab.com/handbook/engineering/infrastructure/test-platform/) team.
- `GITLAB_COOKIE` - Optionally set to "cookie1=value;cookie2=value" in order to add a cookie to every request. This can be used to set the canary cookie by setting it to "gitlab_canary=true".
- `SKIP_CHECK_INSTANCE_AVAILABILITY` - optional - Set environment variable to `true` to force CMBR skipping availability check for target GitLab instance. By default, CMBR attempts to navigate to the host URL of target GitLab instance and ensures that the response code is ok before crawling.

**Controlling traffic rate**: at present, `cmbr` does not perform rate limiting. Instead it uses a single concurrency limiting mechanism. This is configured using the the `-concurrency` command-line argument, defaulting to 10. This will limit the maximum number of concurrent requests that are made in parallel against the GitLab instance. The higher the concurrency, the more requests will be issued.

### Crawling the Web

```console
$ # Set a token for authentication
$ # Note: `-debug` flag for extra verbosity
$ cmbr -concurrency 1 -debug -duration 10s -instance https://staging.gitlab.com/gitlab-org/gitlab
2021/10/22 09:12:53 Visiting https://staging.gitlab.com/gitlab-org/gitlab
2021/10/22 09:12:54 Adding https://staging.gitlab.com/
2021/10/22 09:12:54 Adding https://staging.gitlab.com/explore
...
```

Exceptions:

- Groups and projects under `gitlab-qa-sandbox-group` are skipped as they are being removed quickly and cause 404 errors.
- Downloading archives is disabled to reduce `GetArchive` operations on Staging ([context](https://gitlab.com/gitlab-org/gitlab/-/issues/338978#note_748768951)).

### Crawling Web Assets

This option is similar to the `web` option. It crawls GitLab's web service. In addition to the pages crawled by the
`web` option, this option additionally crawls static asset files (CSS and JS files).

```console
$ # Set a token for authentication
$ cmbr -concurrency 1 -traffic assets -debug -duration 10s -instance https://staging.gitlab.com/gitlab-org/gitlab
2021/10/22 09:12:53 Visiting https://staging.gitlab.com/gitlab-org/gitlab
2021/10/22 09:12:54 Adding https://staging.gitlab.com/
2021/10/22 09:12:54 Adding https://staging.gitlab.com/explore
2021/10/22 09:12:55 Adding https://staging.gitlab.com/assets/stylesheets/stylesheet.css
2021/10/22 09:12:55 Adding https://staging.gitlab.com/assets/vendor/vue.min.js
```

### Crawling the API

```console
$ # Set a token for authentication
$ export GITLAB_TOKEN=password
$ cmbr -concurrency 1 -duration 1m -traffic api -instance https://staging.gitlab.com/api/v4/projects
2021/10/22 09:16:54 Visited 100 URLs
...
```

### Crawling the Git

Crawler goes through existing projects and makes HTTPs requests to `git-upload-pack` and `git-receive-pack` services.

```console
$ # Set a token for authentication
$ export GITLAB_TOKEN=password
$ cmbr -concurrency 1 -duration 1m -traffic git -instance https://staging.gitlab.com/api/v4/projects
2021/10/22 09:16:54 Visited 100 URLs
...
```

### Crawling the Registry

Crawler goes through existing projects and makes HTTPs requests to Registry URLs to invoke requests to the instance's registry.

```console
$ # Set a token for authentication
$ export GITLAB_TOKEN=password
$ cmbr -concurrency 1 -duration 1m -traffic registry -instance https://staging.gitlab.com/api/v4/projects
2021/10/22 09:16:54 Visited 100 URLs
...
```

### Crawling the Pages

⚠️ Note that Pages crawling mode requires specific test data to exist on the target environment.

Crawler loops through Project pages owned by a group to invoke requests to the instance's Pages websites.

```console
$ # Set a token for authentication
$ export GITLAB_TOKEN=password
$ cmbr -concurrency 1 -duration 1m -traffic pages -instance https://pages_test.staging.gitlab.io
2021/10/22 09:16:54 Visited 100 URLs
...
```

CMBR in Pages mode crawls through the provided Page URL and has [AllowURLRevisit](https://pkg.go.dev/github.com/gocolly/colly#AllowURLRevisit) enabled to allow looping through the same set of pages to generate traffic consistently. Please see the information below on how to prepare test data for this.

<details><summary>ℹ️Prepare test data for Pages crawling</summary>

To make crawler loop through the same pages, you need to create a page with a link to itself.

How to prepare test data:

1. Ensure target environment has [GitLab Pages](https://docs.gitlab.com/ee/administration/pages/index.html) configured.
1. Create group to store project pages. For example, named `pages_test`.
1. Create Group page. For this you will need to create a *project* called `<your-group-name>.gitlab.io` under your group using any [project template for Pages](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_new_project_template.html). For example, for `pages_test` group it will be a project called `pages_test.gitlab.io` created under `pages_test` group.
1. Update the created Group page with its Group page URL. For example, `pages_test.gitlab.io` project's code will need to have a link to `pages_test.gitlab.io`. Don't forget to run pipeline to trigger GitLab CI/CD to build and deploy the updates to the site.

The steps above ensure that CMBR will loop through the pages listed on Group page. You may also add more pages under the group and list them on Group page for crawler. For example, create pages projects using [project templates](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_new_project_template.html). Check [GitLab Pages default domain names](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#gitlab-pages-default-domain-names) to learn what URL project page will have. Then list all created Project pages on Group page.

</details>

## Known Uses

1. `cmbr` is used by Instrumentor as a QA verification step: <https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/instrumentor/>
1. Staging load generation jobs are now run in a separate project: <https://gitlab.com/gitlab-com/gl-infra/cmbr-staging-load-generator>, a mirror running these job on staging is available here: <https://staging.gitlab.com/gitlab-com/gl-infra/cmbr-staging-load-generator>.

## Development

`cmbr` is written in Go.

### Hacking on `cmbr`

CMBR uses `asdf` for tool version management. Follow the setup guide at <https://asdf-vm.com/guide/getting-started.html> to install `asdf` on your laptop.

Once you've done this, use the following steps to setup the `cmbr` environment:

1. Follow the developer setup guide at <https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/docs/developer-setup.md>.
1. Clone this project
1. Run `scripts/prepare-dev-env.sh`
1. Your environment is now ready to go!

### Linting

`cmbr` uses `golangci-lint`. This will be installed as an `asdf` plugin.

To lint, run:

```shell
golangci-lint run --fix
```

### Pre-Commit Hooks

`cmbr` uses `pre-commit`: <https://pre-commit.com/>. This can help improve your developer productivity by running checks on your code prior to commit.

This is optional, but recommended.

Follow the instructions at <https://pre-commit.com/#install> to install `pre-commit`, then run `pre-commit install` to setup the required hooks for this project.

## Releases

This project uses [Semantic Releases](https://semantic-release.gitbook.io/semantic-release/). New releases are automatically created based off [Conventional Commit Messages](https://www.conventionalcommits.org/en/v1.0.0/) for commits pushed to the project.

Once merged to the main branch, the `semantic-release` will analyse any new commits since the last release and create a new release accordingly.
