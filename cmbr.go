package main

import (
	"flag"
	"io"
	"os"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	logkit "gitlab.com/gitlab-org/labkit/log"

	"gitlab.com/gitlab-com/gl-infra/cmbr/internal/crawler"
)

type arrayFlags []string

func (i *arrayFlags) String() string {
	return strings.Join(*i, ",")
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, value)

	return nil
}

var parallelism = flag.Int("concurrency", 10, "parallelism of collector")
var instances arrayFlags
var duration = flag.Duration("duration", 60*time.Second, "Runtime of the crawler")
var debug = flag.Bool("debug", false, "Emit debug information")
var traffic = flag.String("traffic", "web", "Traffic options for crawler: one of web, assets, api, git, pages, registry")
var output = flag.String("output", "text", "Output format for logs: one of text, json, color")

func initLogger() io.Closer {
	var logLevel = "info"
	if *debug {
		logLevel = "debug"
	}

	// Initialize the global logger
	closer, err := logkit.Initialize(
		logkit.WithFormatter(*output),   // Use JSON formatting
		logkit.WithLogLevel(logLevel),   // Use info level
		logkit.WithOutputName("stderr"), // Output to stderr
	)
	if err != nil {
		panic(err)
	}

	return closer
}

func main() {
	flag.Var(&instances, "instance", "Starting URL of the GitLab instance to crawl")
	flag.Parse()

	closer := initLogger()
	defer closer.Close()

	if len(instances) == 0 {
		instances = arrayFlags{"https://staging.gitlab.com/api/v4/projects"}
	}

	log.WithFields(log.Fields{"traffic": *traffic}).Info("Traffic flag")

	err := crawler.Crawl(&crawler.Options{
		Parallelism: *parallelism,
		Instances:   instances,
		Debug:       *debug,
		Traffic:     *traffic,

		// Environment configuration
		Username:  os.Getenv("GITLAB_USERNAME"),
		Password:  os.Getenv("GITLAB_PASSWORD"),
		UserAgent: os.Getenv("GITLAB_USER_AGENT"),
		Cookie:    os.Getenv("GITLAB_COOKIE"),
		Token:     os.Getenv("GITLAB_TOKEN"),
	})

	if err != nil {
		log.WithError(err).Fatalf("error crawling")
	}

	time.Sleep(*duration)
}
