#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
ORIGINAL_DIR="$(pwd)"

cd "$SCRIPT_DIR/.."

# Write the code coverage report to gl-code-quality-report.json
# and print linting issues to stdout in the format: path/to/file:line description
# https://docs.gitlab.com/ee/development/go_guide/#automatic-linting
golangci-lint run --timeout 5m --out-format code-climate |
  tee "${ORIGINAL_DIR}/gl-code-quality-report.json" |
  jq -r '.[] | "\(.location.path):\(.location.lines.begin) \(.description)"'
